﻿using UnityEngine;
using Zenject;

namespace Daft.Audio
{
    public class AudioInstaller : MonoInstaller
    {
        public AudioSource      SfxAudioSource;
        public MouthSfxConfig   MouthSfxConfig;

        public override void InstallBindings()
        {
            Container.BindInstance( SfxAudioSource ).WithId( AudioSourcesTags.Sfx );
            Container.Bind<MouthSfxController>().AsSingle().NonLazy();
            Container.BindInstance( MouthSfxConfig.GetDictionary() );
        }
    }
}
