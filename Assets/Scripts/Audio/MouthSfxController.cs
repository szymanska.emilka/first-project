﻿using Daft.MouthImage;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Daft.Audio
{
    public class MouthSfxController
    {
        [Inject(Id = AudioSourcesTags.Sfx)]
        private readonly AudioSource _audioSource;
        [Inject]
        private readonly Dictionary<string, AudioClip> _mouthSfxConfig;

        [Inject]
        private void Init( ChangeImageController changeImageController )
        {
            changeImageController.OnChangeImageRequest += ChangeImageController_OnChangeImageRequest;
        }

        private void ChangeImageController_OnChangeImageRequest( Sprite sprite )
        {
            if( _mouthSfxConfig.ContainsKey(sprite.name))
                _audioSource.PlayOneShot( _mouthSfxConfig[sprite.name]);
        }
    }
}
