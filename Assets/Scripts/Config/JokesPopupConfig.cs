﻿using UnityEngine;

namespace Daft.Joke
{
    [CreateAssetMenu( menuName = "Config/Jokes Popup Config" )]
    public class JokesPopupConfig : ScriptableObject
    {
        public float    TimeBetweenTaps;
        public int      NoOfTapsToShowJoke;
        public float    PopupDuration;
        public string   JokeSourceEndpoint;
    }
}
