﻿using System.Collections.Generic;
using UnityEngine;

namespace Daft.MouthImage
{
    [CreateAssetMenu( menuName = "Config/Mouth Images Config" )]
    public class MouthImagesConfig : ScriptableObject
    {
        public List<Sprite> Sprites;
    }
}
