﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Daft.Audio
{
    [CreateAssetMenu( menuName = "Config/Mouth Sfx Config" )]
    public class MouthSfxConfig : ScriptableObject
    {
        public List<StringAudioPair> SpriteToAudioClip;

        public Dictionary<string, AudioClip> GetDictionary()
        {
            var dict = new Dictionary<string, AudioClip>(SpriteToAudioClip.Count);
            foreach ( var item in SpriteToAudioClip ) {
                dict.Add( item.name, item.audioClip );
            }
            return dict;
        }
    }

    [Serializable]
    public struct StringAudioPair
    {
        public string name;
        public AudioClip audioClip;
    }
}
