﻿namespace Daft.InputUtil
{
    public interface IButtonDownProvider
    {
        bool IsButtonDown();
    }
}
