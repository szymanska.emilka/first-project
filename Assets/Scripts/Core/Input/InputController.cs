﻿using System;
using UnityEngine;
using Zenject;

namespace Daft.InputUtil
{
    public class InputController : MonoBehaviour
    {
        [Inject]
        private readonly IButtonDownProvider _buttonDownProvider;

        public event Action OnButtonDown = delegate { };

        private void Update()
        {
            if ( _buttonDownProvider.IsButtonDown() ) {
                OnButtonDown();
            }
        }
    }
}