﻿using Zenject;

namespace Daft.InputUtil
{
    public class InputInstaller : MonoInstaller
    {
        public InputController InputController;

        public override void InstallBindings()
        {
            Container.Bind<IButtonDownProvider>().To<MouseButtonDownProvider>().AsSingle().NonLazy();
            Container.BindInstance( InputController ).AsSingle().NonLazy();
        }
    }
}
