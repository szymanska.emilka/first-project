﻿using UnityEngine;

namespace Daft.InputUtil
{
    public class MouseButtonDownProvider : IButtonDownProvider
    {
        public bool IsButtonDown()
        {
            return Input.GetKeyDown( KeyCode.Mouse0 );
        }
    }
}
