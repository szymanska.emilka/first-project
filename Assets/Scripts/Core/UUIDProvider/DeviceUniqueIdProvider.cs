﻿using UnityEngine;

namespace Daft.UUID
{
    public class DeviceUniqueIdProvider : IUUIDProvider
    {
        public string GetUUID()
        {
            return SystemInfo.deviceUniqueIdentifier;
        }
    }
}
