﻿namespace Daft.UUID
{
    public interface IUUIDProvider
    {
        string GetUUID();
    }
}
