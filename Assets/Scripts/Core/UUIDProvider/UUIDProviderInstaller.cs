﻿using Zenject;

namespace Daft.UUID
{
    public class UUIDProviderInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IUUIDProvider>().To<DeviceUniqueIdProvider>().AsSingle().NonLazy();
        }
    }
}
