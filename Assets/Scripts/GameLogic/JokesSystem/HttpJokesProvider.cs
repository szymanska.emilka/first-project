﻿using Daft.UUID;
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using IEnumerator = System.Collections.IEnumerator;

namespace Daft.Joke
{
    public class HttpJokesProvider : MonoBehaviour, IJokesProvider
    {
        [Inject]
        private readonly IUUIDProvider      _uuidProvider;
        [Inject]
        private readonly JokesPopupConfig   _jokesConfig;

        private const string _headerKey =  "x-device-uuid";

        public void GetNextJoke( Action<string> callback )
        {
            StartCoroutine( SendRequest( callback ) );
        }

        private IEnumerator SendRequest( Action<string> callback )
        {
            var headers = new Dictionary<string, string>();
            headers.Add( _headerKey, _uuidProvider.GetUUID() );
            using ( WWW www = new WWW( _jokesConfig.JokeSourceEndpoint, null, headers ) ) {
                yield return www;
                if ( !string.IsNullOrEmpty( www.error ) ) {
                    Debug.LogError( "Error while retrieving joke content: " + www.error );
                }
                callback( GetJokeContent( www ) );
            }
        }

        private string GetJokeContent( WWW www )
        {
            return JsonUtility.FromJson<JokeContent>( www.text ).content;
        }
    }
}
