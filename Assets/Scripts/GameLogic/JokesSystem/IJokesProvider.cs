﻿using System;

namespace Daft.Joke
{
    public interface IJokesProvider
    {
        void GetNextJoke( Action<string> callback );
    }
}
