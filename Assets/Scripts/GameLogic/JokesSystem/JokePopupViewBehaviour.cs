﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Zenject;

namespace Daft.Joke
{
    public class JokePopupViewBehaviour : MonoBehaviour
    {
        [Inject]
        private readonly JokesPopupConfig _jokesConfig;

        private GameObject _gameObjectCache;
        private Text       _popupText;
        private Coroutine  _popupHideCoroutine;

        [Inject]
        private void Init()
        {
            var controller = GetComponentInParent<JokesPopupController>();
            Assert.IsNotNull( controller, "JokesPopupViewBehaviour - missing JokesPopupController!" );
            controller.ClosePopup += ClosePopup;
            controller.ShowPopup += ShowPopup;
            _gameObjectCache = gameObject;
            _popupText = GetComponentInChildren<Text>( true );
        }

        private void ClosePopup()
        {
            _gameObjectCache.SetActive( false );
        }

        private void ShowPopup( string jokeText )
        {
            SetPopupView( jokeText );
            StopAlreadyRunningCoroutine();
            HidePopupAfterDelay();
        }

        private void SetPopupView( string jokeText )
        {
            _popupText.text = jokeText;
            _gameObjectCache.SetActive( true );
        }

        private void StopAlreadyRunningCoroutine()
        {
            if ( _popupHideCoroutine != null )
                StopCoroutine( _popupHideCoroutine );
        }

        private void HidePopupAfterDelay()
        {
            _popupHideCoroutine = StartCoroutine( WaitAndHide() );
        }

        private IEnumerator WaitAndHide()
        {
            yield return new WaitForSeconds( _jokesConfig.PopupDuration );
            ClosePopup();
        }
    }
}
