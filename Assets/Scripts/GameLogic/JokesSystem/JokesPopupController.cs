﻿using UnityEngine;
using Zenject;
using System;
using Daft.InputUtil;

namespace Daft.Joke
{
    public class JokesPopupController : MonoBehaviour
    {
        public event Action         ClosePopup  = delegate { };
        public event Action<string> ShowPopup   = delegate { };

        [Inject]
        private readonly JokesPopupConfig   _jokePopupConfig;
        [Inject]
        private readonly IJokesProvider     _jokesProvider;

        private int         currentTaps;
        private DateTime    timeOfLastTap;

        [Inject]
        private void Init( InputController inputController )
        {
            inputController.OnButtonDown += InputController_OnButtonDown;
        }

        private void Awake()
        {
            ClosePopup();
        }

        private void InputController_OnButtonDown()
        {
            ResetCounterIfNeeded();
            IncrementCounter();
            ShowJokeIfNeeded();
        }

        private void ResetCounterIfNeeded()
        {
            if ( ( DateTime.Now - timeOfLastTap ).TotalSeconds > _jokePopupConfig.TimeBetweenTaps ) {
                ResetTapCounters();
            }
        }

        private void IncrementCounter()
        {
            currentTaps++;
            timeOfLastTap = DateTime.Now;
        }

        private void ShowJokeIfNeeded()
        {
            if ( currentTaps >= _jokePopupConfig.NoOfTapsToShowJoke ) {
                ResetTapCounters();
                ShowJokePopup();
            }
        }

        private void ResetTapCounters()
        {
            currentTaps = 0;
        }

        private void ShowJokePopup()
        {
            _jokesProvider.GetNextJoke( ShowPopup );
        }
    }
}
