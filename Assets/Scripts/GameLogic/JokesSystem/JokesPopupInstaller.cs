﻿using Zenject;

namespace Daft.Joke
{
    public class JokesPopupInstaller : MonoInstaller
    {
        public JokesPopupConfig JokesPopupConfig;
        public HttpJokesProvider HttpJokesProvider;

        public override void InstallBindings()
        {
            Container.BindInstance( JokesPopupConfig ).AsSingle().NonLazy();
            Container.BindInstance<IJokesProvider>( HttpJokesProvider ).AsSingle().NonLazy();
        }
    }
}
