﻿using Daft.InputUtil;
using System;
using UnityEngine;
using Zenject;

namespace Daft.MouthImage
{
    public class ChangeImageController
    {
        public event Action<Sprite> OnChangeImageRequest = delegate { };

        [Inject]
        private readonly IImagesProvider _imagesProvider;

        [Inject]
        private void Init( InputController inputController )
        {
            inputController.OnButtonDown += InputController_OnButtonDown;
        }

        private void InputController_OnButtonDown()
        {
            OnChangeImageRequest( _imagesProvider.GetNextImage() );
        }
    }
}
