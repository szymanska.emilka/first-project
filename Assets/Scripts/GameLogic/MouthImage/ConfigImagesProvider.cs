﻿using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Daft.MouthImage
{
    public class ConfigImagesProvider : IImagesProvider
    {
        [Inject]
        private readonly MouthImagesConfig _spritesConfig;

        private int _imageIndex;

        public Sprite GetNextImage()
        {
            Assert.AreNotEqual( _spritesConfig.Sprites.Count, 0, "Sprites config is empty!" );
            return _spritesConfig.Sprites[_imageIndex++ % _spritesConfig.Sprites.Count];
        }
    }
}
