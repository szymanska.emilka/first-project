﻿using UnityEngine;

namespace Daft.MouthImage
{
    public interface IImagesProvider
    {
        Sprite GetNextImage();
    }
}
