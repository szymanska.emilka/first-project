﻿using UnityEngine;
using Zenject;

namespace Daft.MouthImage
{
    [CreateAssetMenu( menuName = "Config/Mouth Images Installer" )]
    public class MouthImagesInstaller : ScriptableObjectInstaller
    {
        public MouthImagesConfig MouthImagesConfig;

        public override void InstallBindings()
        {
            Container.BindInstance( MouthImagesConfig ).AsSingle().NonLazy();
            Container.Bind<IImagesProvider>().To<ConfigImagesProvider>().AsSingle().NonLazy();
            Container.Bind<ChangeImageController>().AsSingle().NonLazy();
        }
    }
}
