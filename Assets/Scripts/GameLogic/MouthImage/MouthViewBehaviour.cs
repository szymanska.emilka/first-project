﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Daft.MouthImage
{
    public class MouthViewBehaviour : MonoBehaviour
    {
        private Image _image;

        [Inject]
        private void Init( ChangeImageController imageController )
        {
            imageController.OnChangeImageRequest += ImageController_OnChangeImageRequest;
            _image = GetComponent<Image>();
        }

        private void ImageController_OnChangeImageRequest( Sprite sprite )
        {
            _image.sprite = sprite;
        }
    }
}
